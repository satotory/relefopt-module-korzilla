<?php

namespace App\modules\Korzilla\Relefopt\Data\Models;

/** поля названы в соответствии с наименованием в базе данных */
class SubClassModel
{
    public $Sub_Class_ID;
    public $Subdivision_ID;
    public $Class_ID;
    public $Sub_Class_Name;
    public $EnglishName;
    public $Catalogue_ID;
    public $Checked;

    public static function modelBySubdivisionModel(SubdivisionModel $subdivisionModel)
    {
        $model = new SubClassModel;

        $model->Sub_Class_ID = NULL;
        $model->Subdivision_ID = $subdivisionModel->Subdivision_ID;
        $model->Class_ID = 2001;
        $model->Sub_Class_Name = $subdivisionModel->Subdivision_Name;
        $model->EnglishName = $subdivisionModel->EnglishName;
        $model->Catalogue_ID = $subdivisionModel->Catalogue_ID;
        $model->Checked = $subdivisionModel->Checked;

        return $model;
    }
}
<?php

namespace App\modules\Korzilla\Relefopt\Data\Models;

use App\modules\Korzilla\Relefopt\Data\DTO\ProductDTO;

class ProductModel
{
    public $Message_ID;

    public $name;
    public $text;
    public $price;
    public $art;
    public $vendor;
    public $code;
    public $stock;
    public $photourl;
    public $Keyword;
    public $Checked;

    public $Sub_Class_ID;
    public $Subdivision_ID;
    public $Subdivision_IDS;
    public $Catalogue_ID;
    public $User_ID;

    public static function modelByDTO(ProductDTO $dto)
    {
        $model = new ProductModel;
        
        $model->name = $dto->name;
        $model->text = $dto->description;
        $model->art = $dto->article;
        $model->vendor = $dto->vendor;
        $model->code = $dto->guid;
        $model->Checked = $dto->checked;
        $model->Catalogue_ID = $dto->catalogueId;
        $model->Keyword = $dto->encodedName;
        $model->Sub_Class_ID = $dto->subClassId;
        $model->Subdivision_IDS = $dto->subdivisionsIds;
        $model->Subdivision_ID = $dto->subdivisionId;
        
        // переделать весь этот блок ниже
        foreach ($dto->pricesArray as $price) {
            if ($price->type == 'recommend') {
                $model->price = round(
                    $price->value + (( $price->value / 100 ) * 56 ), // наценка в 56%, переделать
                    2
                );
            }
        }
        foreach ($dto->stocksArray as $stock) {
            if ($stock->main) {
                $model->stock = $stock->quantity;
            }
        }
        $images = [];
        $i = 1;
        foreach ($dto->imagesArray as $image) {
            if ($image->main) {
                $images[0] = $image->path;
                continue;
            }
            $images[$i] = $image->path;
            $i++;
        }

        $model->photourl = implode(",", $images);
        $model->User_ID = 0;

        return $model;
    }
}
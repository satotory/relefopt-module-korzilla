<?php

namespace App\modules\Korzilla\Relefopt\Data\Models;
use App\modules\Korzilla\Relefopt\Data\DTO\SubdivisionDTO;

/** поля названы в соответствии с наименованием в базе данных */
class SubdivisionModel
{
    public $Subdivision_ID;
    public $Subdivision_Name;
    public $code1C;
    public $text;
    public $Parent_Sub_ID;
    public $Catalogue_ID;
    public $Checked = 1;
    public $EnglishName;
    public $Hidden_URL;

    private $Sub_Class_ID;
    private $parentGuid;

    public static function getModelByArray(array $arr)
    {
        $model = new SubdivisionModel;
        
        $model->Subdivision_ID = $arr['id'] ?? NULL;

        $model->Subdivision_Name = $arr['name'];
        $model->code1C = $arr['guid'];
        $model->text = $arr['description'] ?? "";
        $model->EnglishName = self::encodeString($arr['name']);
        $model->Checked = 1;
        $model->parentGuid = $arr['parentGuid'] ?? NULL;
        $model->Hidden_URL = $arr['Hidden_URL'] ?? NULL;
        $model->Sub_Class_ID = $arr['Sub_Class_ID'] ?? NULL;
        
        return $model;
    }

    public static function modelByDTO(SubdivisionDTO $dto)
    {
        $model = new SubdivisionModel;
        
        $model->Subdivision_ID = $dto->id;
        $model->Subdivision_Name = $dto->name;
        $model->Hidden_URL = $dto->hiddenUrl;
        $model->EnglishName = $dto->encodedName;
        $model->text = $dto->description;
        $model->code1C = $dto->guid;
        $model->Catalogue_ID = $dto->catalogueId;
        $model->Parent_Sub_ID = $dto->parentSubId;

        return $model;
    }

    public function getSubClassID()
    {
        return $this->Sub_Class_ID;
    }

    public static function getModelByStdClass(\stdClass $obj) // избавиться от этого
    {
        $model = new SubdivisionModel;

        $model->Subdivision_ID = $obj->Subdivision_ID;
        $model->Catalogue_ID = $obj->Catalogue_ID;
        $model->Parent_Sub_ID = $obj->Parent_Sub_ID;
        $model->Subdivision_Name = $obj->Subdivision_Name;
        $model->EnglishName = $obj->EnglishName;
        $model->Hidden_URL = $obj->Hidden_URL;
        $model->Checked = $obj->Checked;
        $model->text = $obj->text ?? "";
        $model->code1C = $obj->code1C ?? "";
        
        return $model;
    }

    private static function encodeString($string)
    {
        $table = array(
            'А' => 'a', 'Б' => 'b', 'В' => 'v',
            'Г' => 'g', 'Д' => 'd', 'Е' => 'e',
            'Ё' => 'yo', 'Ж' => 'zh', 'З' => 'z',
            'И' => 'i', 'Й' => 'j', 'К' => 'k',
            'Л' => 'l', 'М' => 'm', 'Н' => 'n',
            'О' => 'o', 'П' => 'p', 'Р' => 'r',
            'С' => 's', 'Т' => 't', 'У' => 'u',
            'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c',
            'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'csh',
            'Ь' => '', 'Ы' => 'y', 'Ъ' => '',
            'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'j', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'csh',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya', '*' => 'x'
        );

        $output = str_replace(array_keys($table), array_values($table), trim($string));
        $output = str_replace("_", "-", $output);
        if (!stristr($output, "http://") && !stristr($output, "https://")) $output = str_replace(" ", "-", trim($output));
        $output = preg_replace("/[^a-zA-Z0-9-]/", "", $output);
        if (is_numeric($output)) $output = "s" . $output;
        
        $output = trim($output, "-");

        return $output;
    }
}
<?php

namespace App\modules\Korzilla\Relefopt\Data\Repositories;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\Models\SubClassModel;

class SubClassRepository
{
    /** @var nc_db */
    private $db;

    /** @var RelefoptConfig */
    private $config;

    private const CLASS_ID = 2001;

    public function __construct(RelefoptConfig $config)
    {
        global $db;

        $this->db = $db;
        $this->config = $config;
    }

    private function getTableName()
    {
        return 'Sub_Class';
    }

    private function getModelClassName()
    {
        return SubClassModel::class;
    }

    public function getAllCatalogsIDs()
    {
        $sqlStatement = "SELECT `Subdivision_ID`, `Sub_Class_ID`
                FROM `Sub_Class` 
                WHERE `Catalogue_ID` = %d
                    AND `Class_ID` = %d";

        $sql = sprintf(
            $sqlStatement,
            $this->config->getCatalogue_id(),
            self::CLASS_ID
        );

        return $this->db->get_results($sql, ARRAY_A);
    }

    public function save(SubClassModel $model)
    {
        if (!$model->Sub_Class_ID) {
            return $this->insert($model);
        } else {
            $this->update($model);
        }
    }

    public function get(int $id)
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE %s = %d", 
            $this->getTableName(),
            $this->mapPropertyToDb('Sub_Class_ID'),
            $id
        );

        if (!$row = $this->db->get_row($sql)) {
            return null;
        }

        return $this->mapRowToModel($row);
    }

    public function getBySubdivision_ID(int $Subdivision_ID)
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE %s = %d", 
            $this->getTableName(),
            $this->mapPropertyToDb('Subdivision_ID'),
            $Subdivision_ID
        );

        if (!$row = $this->db->get_row($sql)) {
            return null;
        }

        return $this->mapRowToModel($row);
    }

    private function insert(SubClassModel $model)
    {
        $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", 
            $this->getTableName(), 
            ...$this->modelToInsert($model)
        );
        
        $this->db->query($sql);

        $model->Sub_Class_ID = $this->db->insert_id;

        if (!$model->Sub_Class_ID) {
            echo "<pre>";
            var_dump($model, $this->db->last_error);
            echo "</pre>";
            throw new \Exception('Не удалось создать cc.');
        }

        return $model->Sub_Class_ID;
    }

    private function update(SubClassModel $model): void
    {
        $sql = sprintf("UPDATE `%s` SET %s WHERE %s = %d", 
            $this->getTableName(), 
            $this->modelToUpdate($model),
            $this->mapPropertyToDb('Sub_Class_ID'),
            $model->Sub_Class_ID
        );

        $this->db->query($sql);
    }

    private function modelToInsert(SubClassModel $model): array
    {
        $fields = [];
        $values = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'Sub_Class_ID') continue;

            $fields[] = $this->mapPropertyToDb($prop);
            $values[] = $this->mapValueToDb($value);
        }

        return [
            implode(',', $fields),
            implode(',', $values),
        ];
    }

    private function modelToUpdate(SubClassModel $model): string
    {
        $set = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'Sub_Class_ID') continue;

            $set[] = sprintf('%s = %s', $this->mapPropertyToDb($prop), $this->mapValueToDb($value));
        }

        return implode(',', $set);
    }

    private function mapPropertyToDb(string $property): string
    {
        return sprintf('%s.`%s`', $this->getTableName(), $property);
    }

    private function mapValueToDb($value): string
    {
        if ($value === null) {
            return 'NULL';
        }

        if (is_int($value)) {
            return "{$value}";
        }

        return "'{$value}'";
    }

    private function mapRowToModel(\stdClass $row): SubClassModel
    {
        $model = $this->getModel();

        foreach ($row as $field => $value) {
            $model->$field = $value;
        }

        return $model;
    }

    private function getModel(): SubClassModel
    {
        $class = $this->getModelClassName();

        return new $class;
    }
}
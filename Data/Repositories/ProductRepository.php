<?php

namespace App\modules\Korzilla\Relefopt\Data\Repositories;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\Models\ProductModel;

class ProductRepository
{
    /** @var nc_db */
    private $db;

    /** @var RelefoptConfig */
    private $config;

    public function __construct(RelefoptConfig $config)
    {
        global $db;

        $this->db = $db;
        $this->config = $config;
    }

    private function getTableName()
    {
        return 'Message2001';
    }

    private function getModelClassName()
    {
        return ProductModel::class;
    }

    public function save(ProductModel $model)
    {
        if (!$model->Message_ID) {
            $this->insert($model);
        } else {
            $this->update($model);
        }
    }

    public function get(int $id)
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE %s = %d", 
            $this->getTableName(),
            $this->mapPropertyToDb('Message_ID'),
            $id
        );

        if (!$row = $this->db->get_row($sql)) {
            return null;
        }

        return $this->mapRowToModel($row);
    }

    public function getByCode(string $code)
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE %s = %d AND %s = %s", 
            $this->getTableName(),
            $this->mapPropertyToDb('Catalogue_ID'),
            $this->mapValueToDb($this->config->getCatalogue_id()),
            $this->mapPropertyToDb('code'),
            $this->mapValueToDb($code)
        );

        if (!$row = $this->db->get_row($sql)) {
            return null;
        }

        return $this->mapRowToModel($row);
    }

    private function insert(ProductModel $model): void
    {
        $sql = sprintf("INSERT INTO `%s` (%s) VALUES (%s)", 
            $this->getTableName(), 
            ...$this->modelToInsert($model)
        );
        
        $this->db->query($sql);

        $model->Message_ID = $this->db->insert_id;

        if (!$model->Message_ID) {
            // Логгер должен быть тут
            // echo "<pre>";
            // var_dump($model, $this->db->last_error);
            // echo "</pre>";
            // file_put_contents(__DIR__ . "/logs/productsInsertErrorLog.log", print_r([
            //     "sql" => $sql,
            //     "model" => $model, 
            //     "error" => $this->db->last_error
            // ], 1), FILE_APPEND);
        }
    }

    private function update(ProductModel $model): void
    {
        $sql = sprintf("UPDATE `%s` SET %s WHERE %s = %d", 
            $this->getTableName(), 
            $this->modelToUpdate($model),
            $this->mapPropertyToDb('Message_ID'),
            $model->Message_ID
        );
        
        // Логгер должен быть тут
        // file_put_contents(__DIR__ . "/logs/productsLog.log", print_r("\n".$sql."\n", 1), FILE_APPEND);

        $this->db->query($sql);
    }

    private function modelToInsert(ProductModel $model): array
    {
        $fields = [];
        $values = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'Message_ID') continue;

            $fields[] = $this->mapPropertyToDb($prop);
            $values[] = $this->mapValueToDb($value);
        }

        return [
            implode(',', $fields),
            implode(',', $values),
        ];
    }

    private function modelToUpdate(ProductModel $model): string
    {
        $set = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'Message_ID') continue;

            $set[] = sprintf('%s = %s', $this->mapPropertyToDb($prop), $this->mapValueToDb($value));
        }

        return implode(',', $set);
    }

    private function mapPropertyToDb(string $property): string
    {
        return sprintf('%s.`%s`', $this->getTableName(), $property);
    }

    private function mapValueToDb($value): string
    {
        if ($value === null) {
            return 'NULL';
        }

        if (is_int($value)) {
            return "{$value}";
        }

        return "'{$value}'";
    }

    private function mapRowToModel(\stdClass $row): ProductModel
    {
        $model = $this->getModel();

        foreach ($row as $field => $value) {
            $model->$field = $value;
        }

        return $model;
    }

    private function getModel(): ProductModel
    {
        $class = $this->getModelClassName();

        return new $class;
    }
}
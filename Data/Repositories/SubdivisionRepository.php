<?php

namespace App\modules\Korzilla\Relefopt\Data\Repositories;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\Models\SubClassModel;
use App\modules\Korzilla\Relefopt\Data\Models\SubdivisionModel;

class SubdivisionRepository
{
    /** @var nc_db */
    private $db;

    /** @var RelefoptConfig */
    private $config;

    /** @var SubClassRepository */
    private $ccRepository;

    public function __construct(RelefoptConfig $config)
    {
        global $db;

        $this->db = $db;
        $this->config = $config;
        $this->ccRepository = new SubClassRepository($config);
    }

    private function getTableName()
    {
        return 'Subdivision';
    }

    private function getModelClassName()
    {
        return SubdivisionModel::class;
    }

    public function getAllCatalogs()
    {
        
        $subClasses = $this->ccRepository->getAllCatalogsIDs();

        $subdivisionsIds = [];
        foreach ($subClasses as $sub) {
            $subdivisionsIds[] = $sub['Subdivision_ID'];
            $subClassIdsadasd[$sub['Subdivision_ID']] = $sub['Sub_Class_ID'];
        }

        $sqlStatement = "SELECT `Subdivision_ID` as id, `code1C` as guid, `Subdivision_Name` as name, `Hidden_URL`
                        FROM `Subdivision` 
                        WHERE `Catalogue_ID` = %d
                            AND `code1C` != ''
                            AND `Subdivision_ID` IN (%s)
                            AND `Subdivision_Name` NOT IN (%s)";

        $bannedSubs = ['\'Поиск по каталогу\'', '\'Бренды\'', '\'Сравнение товаров\'', '\'Вывод уникальных товаров для сео\'', '\'Каталог\'', '\'Спецпредложения\'', '\'Избранное\'', '\'Новинки\'', '\'Хиты продаж\'',];
        $sql = sprintf(
            $sqlStatement,
            $this->config->getCatalogue_id(),
            implode(",", $subdivisionsIds),
            implode(",", $bannedSubs)
        );
        
        $result = $this->db->get_results($sql, ARRAY_A);
        if (!$result) {
            return [];
        }
        
        $subdivisions = [];
        foreach ($result as $sub) {
            $sub['Sub_Class_ID'] = $subClassIdsadasd[$sub['id']];
            $subdivisions[$sub['guid']] = SubdivisionModel::getModelByArray($sub);
        }
        
        return $subdivisions;
    }

    public function save(SubdivisionModel $model)
    {
        if (!$model->Subdivision_ID) {
            return $this->insert($model);
        } else {
            return $this->update($model);
        }
    }

    public function get(int $id)
    {
        $sql = sprintf("SELECT * FROM `%s` WHERE %s = %d", 
            $this->getTableName(),
            $this->mapPropertyToDb('Subdivision_ID'),
            $id
        );

        if (!$row = $this->db->get_row($sql)) {
            return null;
        }

        return $this->mapRowToModel($row);
    }

    private function insert(SubdivisionModel $model)
    {
        $sql = sprintf("INSERT INTO `%s` \n(%s) VALUES \n(%s)", 
            $this->getTableName(), 
            ...$this->modelToInsert($model)
        );

        $this->db->query($sql);

        $model->Subdivision_ID = $this->db->insert_id;
        if (!$model->Subdivision_ID) {
            echo "<pre>";
            var_dump($model, $this->db->last_error);
            echo "</pre>";
        }

        if ($model->Subdivision_ID) {
            $ccId = $this->ccRepository->save(SubClassModel::modelBySubdivisionModel($model));
        }

        return ['subId' => $model->Subdivision_ID, 'ccId' => $ccId] ?? null;
    }

    private function update(SubdivisionModel $model): void
    {
        $sql = sprintf("UPDATE `%s` SET %s WHERE %s = %d", 
            $this->getTableName(), 
            $this->modelToUpdate($model),
            $this->mapPropertyToDb('Subdivision_ID'),
            $model->Subdivision_ID
        );

        $this->db->query($sql);
    }

    private function modelToInsert(SubdivisionModel $model): array
    {
        $fields = [];
        $values = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'Subdivision_ID') continue;

            $fields[] = $this->mapPropertyToDb($prop);
            $values[] = $this->mapValueToDb($value);
        }

        return [
            implode(',', $fields),
            implode(',', $values),
        ];
    }

    private function modelToUpdate(SubdivisionModel $model): string
    {
        $set = [];

        foreach ($model as $prop => $value) {
            if ($prop === 'Subdivision_ID') continue;

            $set[] = sprintf('%s = %s', $this->mapPropertyToDb($prop), $this->mapValueToDb($value));
        }

        return implode(',', $set);
    }

    private function mapPropertyToDb(string $property): string
    {
        return sprintf('%s.`%s`', $this->getTableName(), $property);
    }

    private function mapValueToDb($value): string
    {
        if ($value === null) {
            return 'NULL';
        }

        if (is_int($value)) {
            return "{$value}";
        }

        return "'{$value}'";
    }

    private function mapRowToModel(\stdClass $row): SubdivisionModel
    {
        $model = $this->getModel()::getModelByStdClass($row);

        return $model;
    }

    private function getModel(): SubdivisionModel
    {
        $class = $this->getModelClassName();

        return new $class;
    }
}
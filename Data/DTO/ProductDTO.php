<?php

namespace App\modules\Korzilla\Relefopt\Data\DTO;

class ProductDTO
{
    /** @var int */
    public $id = NULL;

    /** @var int */
    public $checked = 0;

    /** @var string */
    public $name = NULL;

    /** @var string */
    public $encodedName = NULL;

    /** @var string */
    public $guid = NULL;

    /** @var string */
    public $description = NULL;

    /** @var string */
    public $article;

    /** @var string */
    public $vendor = NULL;
    
    /** @var int */
    public $subdivisionId = NULL;

    /** @var int */
    public $subClassId = NULL;
    
    /** @var int */
    public $catalogueId = NULL;
    
    /** @var string */
    public $subdivisionsIds = NULL;

    /** @var StockDTO[] */
    public $stocksArray;

    /** @var PriceDTO[] */
    public $pricesArray;

    /** @var ImageDTO[] */
    public $imagesArray = [];
}
<?php

namespace App\modules\Korzilla\Relefopt\Data\DTO;

use App\modules\Korzilla\Relefopt\Data\Models\SubdivisionModel;
use App\modules\Korzilla\Relefopt\Data\Models\SubClassModel;

class RootCatalogDTO
{
    /** @var SubdivisionModel */
    public $rootSubdivision;

    /** @var SubClassModel */
    public $rootSubClass;
}
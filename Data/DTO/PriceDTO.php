<?php

namespace App\modules\Korzilla\Relefopt\Data\DTO;

class PriceDTO
{
    /** @var string */
    public $type = NULL;
    
    /** @var float */
    public $value = NULL;
}
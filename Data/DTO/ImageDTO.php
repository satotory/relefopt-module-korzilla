<?php

namespace App\modules\Korzilla\Relefopt\Data\DTO;

class ImageDTO
{
    /** @var string */
    public $path;
    
    /** @var bool */
    public $main = false;
}
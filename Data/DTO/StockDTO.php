<?php

namespace App\modules\Korzilla\Relefopt\Data\DTO;

class StockDTO
{
    /** @var string */
    public $store = NULL;
    
    /** @var bool */
    public $main = false;

    /** @var string */
    public $code = NULL;

    /** @var int */
    public $quantity = NULL;
}
<?php

namespace App\modules\Korzilla\Relefopt\Data\DTO;

class SubdivisionDTO
{
    /** @var int */
    public $id = NULL;

    /** @var string */
    public $name = NULL;

    /** @var string */
    public $guid = NULL;

    /** @var string */
    public $description = NULL;

    /** @var string */
    public $encodedName = NULL;

    /** @var string */
    public $parentGuid = NULL;

    /** @var string */
    public $hiddenUrl = NULL;

    /** @var int */
    public $level;

    /** @var string */
    public $insertOrUpdate;

    /** @var int */
    public $subClassId;

    /** @var int */
    public $parentSubId;

    /** @var int */
    public $catalogueId;
}
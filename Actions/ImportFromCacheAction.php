<?php

namespace App\modules\Korzilla\Relefopt\Actions;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Tasks\GetCatalogsListFromCacheTask;
use App\modules\Korzilla\Relefopt\Tasks\GetProductDTOFromProductInfoArray;
use App\modules\Korzilla\Relefopt\Tasks\GetRootCatalogDTOTask;
use App\modules\Korzilla\Relefopt\Tasks\RemapCatalogsListTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveCatalogsListToDatabaseTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveProductToDatabaseTask;

class ImportFromCacheAction
{
    /** @var RelefoptConfig */
    private $config;

    /** @var GetCatalogsListFromCacheTask */
    private $getCatalogsListFromCacheTask;

    /** @var RemapCatalogsListTask */
    private $remapCatalogsListTask;

    /** @var GetRootCatalogDTOTask */
    private $getRootCatalogDTOTask;

    /** @var SaveCatalogsListToDatabaseTask */
    private $saveCatalogsListToDatabaseTask;

    /** @var GetProductDTOFromProductInfoArray */
    private $getProductDTOFromProductInfoArray;

    /** @var SaveProductToDatabaseTask */
    private $saveProductToDatabaseTask;

    public function __construct(
        RelefoptConfig $config,
        GetCatalogsListFromCacheTask $getCatalogsListFromCacheTask,
        RemapCatalogsListTask $remapCatalogsListTask,
        GetRootCatalogDTOTask $getRootCatalogDTOTask,
        SaveCatalogsListToDatabaseTask $saveCatalogsListToDatabaseTask,
        GetProductDTOFromProductInfoArray $getProductDTOFromProductInfoArray,
        SaveProductToDatabaseTask $saveProductToDatabaseTask
    ) {
        $this->config = $config;
        $this->getCatalogsListFromCacheTask = $getCatalogsListFromCacheTask;
        $this->remapCatalogsListTask = $remapCatalogsListTask;
        $this->getRootCatalogDTOTask = $getRootCatalogDTOTask;
        $this->saveCatalogsListToDatabaseTask = $saveCatalogsListToDatabaseTask;
        $this->getProductDTOFromProductInfoArray = $getProductDTOFromProductInfoArray;
        $this->saveProductToDatabaseTask = $saveProductToDatabaseTask;
    }

    public function run()
    {
        $time_start = microtime(true);

        $catalogsRelations = $this->importCatalogs();
        if (gettype($catalogsRelations) != 'array') {
            throw new \Exception("Нет связей разделов");
        }
        
        $this->importProducts($catalogsRelations);

        $et = microtime(true) - $time_start;

        echo "<pre><i>Execution Time : " . $et . "sec</i></pre>";
    }

    public function importCatalogs()
    {
        return $this->saveCatalogsListToDatabaseTask->run(
            $this->remapCatalogsListTask->run(
                $this->getCatalogsListFromCacheTask->run(),
                $this->getRootCatalogDTOTask->run($this->config->getRootCatalogId())
            )
        );
    }

    public function importProducts(array $catalogsRelations)
    {
        $memory_start = memory_get_usage();
        
        $time_start = microtime(true);
        $this->flushStart();

        foreach ($this->getProductsListFromCacheFile() as $productsList) {
            $time_start_i = microtime(true);

            foreach ($productsList as $productInfo) {
                $this->saveProductToDatabaseTask->run(
                    $this->getProductDTOFromProductInfoArray->run($productInfo, $catalogsRelations)
                );
            }

            $this->flushExecutionTime($time_start_i);

            $this->flushMemoryUsageInfo($memory_start);
        }

        $this->flushEnd($time_start);
    }

    private function getProductsListFromCacheFile() : iterable
    {
        $cacheFiles = array_diff(
            scandir( $this->config->getProductsCacheFolder(), SCANDIR_SORT_NONE ), 
            ['.', '..']
        );

        foreach ($cacheFiles as $cacheFile) {
            $cache = json_decode(
                file_get_contents($this->config->getProductsCacheFolder() . $cacheFile), 1
            );
            if (gettype($cache) != 'array') {
                throw new \Exception("В файле должен быть массив товаров, его там нет. Ошибка.");
            }
            
            $this->flushFileName($cacheFile);

            yield $cache;
        }
    }

    private function convert($size) // delete
    {
       $unit=array('b','kb','mb','gb','tb','pb');
       return @round($size/pow(1024,($i=floor(log($size,1024)))),2).' '.$unit[$i];
    }

    private function flushMemoryUsageInfo(float $memory_start)
    {
        echo str_pad('',4096) . "\n";
        echo "\n      <b>start:" . $this->convert($memory_start) . "end memUsage: " . $this->convert(memory_get_usage() - $memory_start) . "peak:" . $this->convert(memory_get_peak_usage()) . "</b>\n";
        echo str_pad('',4096) . "\n";
        ob_flush();
        flush();
    }

    private function flushStart()
    {
        if (ob_get_level() == 0) ob_start();
        echo "<pre>";
        echo "Выгрузка товаров:" . "\n";
        echo str_pad('',4096) . "\n";
        ob_flush();
        flush();
    }

    private function flushFileName(string $filename)
    {
        echo "      Выгрузка товаров <b>" . $filename . "</b>\n";
        echo str_pad('',4096) . "\n";
        ob_flush();
        flush();
    }

    private function flushExecutionTime(float $time_start)
    {
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);

        echo "      Выгрузка чанка товаров закончена. <b>Заняло " . $execution_time . " секунд</b>";
    }

    private function flushEnd(float $time_start)
    {
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);

        echo "\nВыгрузка товаров закончена. <b>Общее время " . $execution_time . " секунд</b>";
        echo "</pre>";
        ob_end_flush();
    }
}
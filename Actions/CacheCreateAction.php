<?php

namespace App\modules\Korzilla\Relefopt\Actions;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Tasks\GetCatalogsListFromApiTask;
use App\modules\Korzilla\Relefopt\Tasks\GetProductsListFromApiTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveArrayToJSONCacheTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveArrayToJSONLogTask;

class CacheCreateAction
{
    /** @var GetCatalogsListFromApiTask */
    private $getCatalogsListFromApiTask;

    /** @var GetProductsListFromApiTask */
    private $getProductsListFromApiTask;

    /** @var SaveArrayToJSONCacheTask */
    private $saveArrayToJSONCacheTask;

    /** @var SaveArrayToJSONLogTask */
    private $saveArrayToJSONLogTask;

    /** @var RelefoptConfig */
    private $config;

    public function __construct(
        RelefoptConfig $config,
        GetCatalogsListFromApiTask $getCatalogsListFromApiTask,
        GetProductsListFromApiTask $getProductsListFromApiTask,
        SaveArrayToJSONCacheTask $saveArrayToJSONCacheTask,
        SaveArrayToJSONLogTask $saveArrayToJSONLogTask
    ) {
        $this->config = $config;
        $this->getCatalogsListFromApiTask = $getCatalogsListFromApiTask;
        $this->getProductsListFromApiTask = $getProductsListFromApiTask;
        $this->saveArrayToJSONCacheTask = $saveArrayToJSONCacheTask;
        $this->saveArrayToJSONLogTask = $saveArrayToJSONLogTask;
    }

    public function run()
    {
        // $this->catalogsCacheCreate();
        $this->productsCacheCreate();
    }

    private function catalogsCacheCreate()
    {
        $this->saveArrayToJSONCacheTask->run(
            $this->getCatalogsListFromApiTask->run(),
            $this->config->getCacheFolder(), 
            $this->config->getCatalogsCacheFileName()
        );
    }

    private function productsCacheCreate()
    {
        /** @var int количество доступных товаров */
        $productsCount = $this->getProductsListFromApiTask->getCount();
        
        $time_start = microtime(true);
        $this->startProductsCachingDisplay($productsCount);

        $limit = 500;
        /** @var int количество секунд остановки при неудачной попытке забрать товары из API */
        $sleep_seconds = 5;
        /** @var int количество попыток забрать товары из API */
        $triesLimit = 3;
        for ($offset = 0; $offset < $productsCount; $offset += $limit) {
            $iteration_time_start = microtime(true);

            $filenameSuffix = sprintf("%d-%d", $offset, $offset + $limit);

            for ($tries = 0; $tries < $triesLimit; $tries++) {
                /** @var array|null массив товаров */
                $productsArray = $this->getProductsListFromApiTask->run($limit, $offset);
                if (!$productsArray) {
                    $this->creatingProductsCacheExceptionGetData();
                    
                    $exceptionData = $this->getProductsListFromApiTask->getLastExceptionDataFromRequest();
                    $exceptionFilenameSuffix = sprintf("%s_try-%d", $filenameSuffix, $tries);

                    $this->saveArrayToJSONLogTask->run(
                        $exceptionData,
                        $this->config->getProductsLogFileName($exceptionFilenameSuffix)
                    );

                    sleep($sleep_seconds);
                    continue;
                }

                break;
            }

            $this->saveArrayToJSONCacheTask->run(
                $productsArray,
                $this->config->getProductsCacheFolder(),
                $this->config->getProductsCacheFileName($filenameSuffix)
            );

            $this->creatingProductsCacheExecutionTimeDisplay(
                $filenameSuffix,
                (microtime(true) - $iteration_time_start)
            );
        }
        
        $this->endProductsCachingDisplay($productsCount, $time_start);
    }

    private function startProductsCachingDisplay(int $productsCount)
    {
        if (ob_get_level() == 0) ob_start();
        echo "<pre>";
        echo "Количество товаров: ". $productsCount . ". Создание кэша товаров:" . "\n";
        echo str_pad('',4096) . "\n";
        ob_flush();
        flush();

        return microtime(true);
    }

    private function creatingProductsCacheExecutionTimeDisplay(string $filenameSuffix, float $iteration_execution_time)
    {
        echo "      Создание кэша товаров " . $filenameSuffix . " заняло " . $iteration_execution_time . " секунд\n";
        echo str_pad('',4096) . "\n";
        ob_flush();
        flush();
    }

    private function endProductsCachingDisplay(int $productsCount, float $time_start)
    {
        $time_end = microtime(true);
        $execution_time = ($time_end - $time_start);

        echo "\nСоздание кэша ". $productsCount ." товаров заняло " . $execution_time . " секунд";
        echo "</pre>";
        ob_end_flush();
    }

    private function creatingProductsCacheExceptionGetData()
    {
        echo "       Не получил корректный ответ. Остановка. Делаю повторный запрос.\n";
        echo str_pad('',4096) . "\n";
        ob_flush();
        flush();
    }
}
<?php

namespace App\modules\Korzilla\Relefopt\Config;

class RelefoptConfig
{
    /** @var int */
    private $rootCatalogId;
    
    /** @var int */
    private $rootSubClassId; // исправить этот костыль

    /** @var int */
    private $catalogue_id;

    /** @var string */
    private $apiKey;

    private $classification;
    private const STANDARD_CLASSIFICATION = "2fa5c39d-d2fd-11ea-80c2-30e1715c5317";
    private const CACHE_FOLDER = __DIR__ . "/../var/cache/";
    private const LOG_FOLDER = __DIR__ . "/../var/log/";
    private const PRODUCTS_CACHE_FOLDER = "products/";

    public function __construct(
        string $apiKey,
        int $rootCatalogId, 
        int $rootSubClassId,
        int $catalogue_id
    ) {
        $this->apiKey = $apiKey;
        $this->rootCatalogId = $rootCatalogId;
        $this->catalogue_id = $catalogue_id;
        $this->rootSubClassId = $rootSubClassId;
    }

    /**
     * @return string
     */
    public function getApiKey()
    {
        return $this->apiKey;
    }

    /**
	 * @return string
	 */
	public function getClassification() {
		return $this->classification ?: self::STANDARD_CLASSIFICATION;
	}
	
	/**
	 * @param string $classification 
	 * @return self
	 */
	public function setClassification($classification): self {
		$this->classification = $classification;

		return $this;
	}

    /**
	 * @return string
	 */
	public function getCacheFolder() {
        $todaysFolder = date("ymd");

		return self::CACHE_FOLDER . "/" . $todaysFolder . "/";
	}

    /**
	 * @return string
	 */
	public function getProductsCacheFolder() {
        $todaysFolder = date("ymd");

		return self::CACHE_FOLDER . "/" . $todaysFolder . "/" . self::PRODUCTS_CACHE_FOLDER;
	}

     /**
	 * @return string
	 */
	public function getLogFolder() {
        $todaysFolder = date("ymd");

		return self::CACHE_FOLDER . "/" . $todaysFolder . "/";
	}

    public function getProductsLogFileName(string $filenameSuffix = "")
    {
        $filenamePrefix = "e_products";
        $fileFormat = "json";

        return sprintf(
            "%s_%s.%s",
            $filenamePrefix,
            $filenameSuffix,
            $fileFormat
        );
    }

    public function getCatalogsCacheFileName(string $filenameSuffix = "")
    {
        $filenamePrefix = "catalogs";
        $fileFormat = "json";

        return sprintf(
            "%s_%s.%s",
            $filenamePrefix,
            $filenameSuffix,
            $fileFormat
        );
    }

    public function getProductsCacheFileName(string $filenameSuffix = "")
    {
        $filenamePrefix = "products";
        $fileFormat = "json";

        return sprintf(
            "%s_%s.%s",
            $filenamePrefix,
            $filenameSuffix,
            $fileFormat
        );
    }

	/**
	 * @return int
	 */
	public function getRootCatalogId() 
    {
		return $this->rootCatalogId;
	}

    /**
	 * @return int
	 */
	public function getRootSubClassId() 
    {
		return $this->rootSubClassId;
	}

	/**
	 * @return int
	 */
	public function getCatalogue_id() 
    {
		return $this->catalogue_id;
	}
}
<?php

namespace App\modules\Korzilla\Relefopt\UI\Web\Controllers;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Factories\CacheCreateActionFactory;
use App\modules\Korzilla\Relefopt\Factories\ImportFromCacheActionFactory;

class ImportCatalogFromRelefoptController
{
    /**
     * Создает кэш каталогов и товаров
     */
    public function createCache()
    {
        // исправить этот блок
        $apiKey = "";
        $rootCatalogId = 344169;
        $rootCC = 415824;
        $catalogueId = 730;

        $config = new RelefoptConfig($apiKey, $rootCatalogId, $rootCC, $catalogueId);

        (CacheCreateActionFactory::get($config))->run();
    }

    public function importFromCache()
    {
        // исправить этот блок
        $apiKey = "";
        $rootCatalogId = 344169;
        $rootCC = 415824;
        $catalogueId = 730;

        $config = new RelefoptConfig($apiKey, $rootCatalogId, $rootCC, $catalogueId);

        (ImportFromCacheActionFactory::get($config))->run();
    }
}
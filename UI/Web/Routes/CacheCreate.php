<?php
ini_set('memory_limit', '1600M');
set_time_limit(1000000);

require_once $_SERVER['DOCUMENT_ROOT'] . "/vars.inc.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/connect_io.php";
require_once $_SERVER['DOCUMENT_ROOT'] . "/bc/modules/default/function.inc.php";

ini_set('display_errors', '1');
ini_set('display_startup_errors', '1');
error_reporting(E_ALL);

use App\modules\Korzilla\Relefopt\UI\Web\Controllers\ImportCatalogFromRelefoptController;

$controller = new ImportCatalogFromRelefoptController;

$controller->createCache();
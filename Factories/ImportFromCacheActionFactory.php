<?php

namespace App\modules\Korzilla\Relefopt\Factories;

use App\modules\Korzilla\Relefopt\Actions\ImportFromCacheAction;
use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\Repositories\ProductRepository;
use App\modules\Korzilla\Relefopt\Data\Repositories\SubClassRepository;
use App\modules\Korzilla\Relefopt\Data\Repositories\SubdivisionRepository;
use App\modules\Korzilla\Relefopt\Tasks\GetCatalogsListFromCacheTask;
use App\modules\Korzilla\Relefopt\Tasks\GetProductDTOFromProductInfoArray;
use App\modules\Korzilla\Relefopt\Tasks\GetRootCatalogDTOTask;
use App\modules\Korzilla\Relefopt\Tasks\RemapCatalogsListTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveCatalogsListToDatabaseTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveProductToDatabaseTask;

class ImportFromCacheActionFactory
{
    private static $instance;

    public static function get(RelefoptConfig $config): ImportFromCacheAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(RelefoptConfig $config): ImportFromCacheAction
    {
        $subRepository = new SubdivisionRepository($config);
        $ccRepository = new SubClassRepository($config);
        $productRepository = new ProductRepository($config);

        $getRootCatalogDTOTask = new GetRootCatalogDTOTask($subRepository, $ccRepository);
        $getCatalogsListFromApiTask = new GetCatalogsListFromCacheTask($config);
        $remapCatalogListTask = new RemapCatalogsListTask($subRepository, $config);
        $saveCatalogListToDatabase = new SaveCatalogsListToDatabaseTask($config, $subRepository, $ccRepository);
        
        $getProductDTOFromProductInfoArray = new GetProductDTOFromProductInfoArray($config);
        $saveProductsToDatabaseTask = new SaveProductToDatabaseTask($config, $productRepository);

        return new ImportFromCacheAction(
            $config,
            $getCatalogsListFromApiTask,
            $remapCatalogListTask,
            $getRootCatalogDTOTask,
            $saveCatalogListToDatabase,
            $getProductDTOFromProductInfoArray,
            $saveProductsToDatabaseTask
        );
    }
}
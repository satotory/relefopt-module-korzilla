<?php

namespace App\modules\Korzilla\Relefopt\Factories;

use App\modules\Korzilla\Relefopt\Actions\CacheCreateAction;
use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Tasks\GetCatalogsListFromApiTask;
use App\modules\Korzilla\Relefopt\Tasks\GetProductsListFromApiTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveArrayToJSONCacheTask;
use App\modules\Korzilla\Relefopt\Tasks\SaveArrayToJSONLogTask;

class CacheCreateActionFactory
{
    private static $instance;

    public static function get(RelefoptConfig $config): CacheCreateAction
    {
        if (!self::$instance) {
            self::$instance = self::create($config);
        }

        return self::$instance;
    }

    private static function create(RelefoptConfig $config): CacheCreateAction
    {
        $getCatalogsListFromApiTask = new GetCatalogsListFromApiTask($config);
        $getProductsListFromApiTask = new GetProductsListFromApiTask($config);
        $saveArrayToJSONCacheTask = new SaveArrayToJSONCacheTask($config);
        $saveArrayToJSONLogTask = new SaveArrayToJSONLogTask($config);

        return new CacheCreateAction(
            $config,
            $getCatalogsListFromApiTask,
            $getProductsListFromApiTask,
            $saveArrayToJSONCacheTask,
            $saveArrayToJSONLogTask
        );
    }
}
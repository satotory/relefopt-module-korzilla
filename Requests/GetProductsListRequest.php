<?php

namespace App\modules\Korzilla\Relefopt\Requests;

class GetProductsListRequest
{
    private const API_URL = "https://api-sale.relef.ru";

    /** 
     * GET /api/v1/sections/:catalogGuid/list
     * @var string 
     */
    private const METHOD = "api/v1/products/list";
    private $lastExceptionData;

    private function getUrl()
    {
        return self::API_URL . "/" . self::METHOD;
    }

    private function getHeaders(string $apiKey)
    {
        return [ "apikey: " . $apiKey, "'Content-Type: application/json'", ];
    }

    public function run(string $apiKey, int $limit, int $offset) 
    {
        $body["filter"] = [ "availability" => true, ];
        $body["limit"] = $limit;
        $body["offset"] = $offset;

        $reply = $this->curl_post(
            $this->getUrl(), 
            $this->getHeaders($apiKey), 
            $body
        );
        if (!$reply) {
            $this->lastExceptionData = [
                "method" => $this->getUrl(),
                "body" => $body,
                "exception_reply" => $reply,
            ];
        }

        return $reply;
    }

    public function getLastExceptionData()
    {
        return $this->lastExceptionData;
    }

    private function curl_post(string $url, array $headers, array $data) {
        $ch=curl_init();

        curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch,CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch,CURLOPT_HEADER, false);
        curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch,CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch,CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($data, JSON_UNESCAPED_UNICODE));

        $reply=curl_exec($ch);
        curl_close($ch);
        
        return $reply;
    }
}
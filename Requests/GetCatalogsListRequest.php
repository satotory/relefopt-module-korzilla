<?php

namespace App\modules\Korzilla\Relefopt\Requests;

class GetCatalogsListRequest
{
    private const API_URL = "https://api-sale.relef.ru";

    /** 
     * GET /api/v1/sections/:catalogGuid/list
     * @var string 
     */
    private const METHOD = "api/v1/sections/%s/list";

    private function getUrl(string $classification)
    {
        return self::API_URL . "/" . sprintf(self::METHOD, $classification);
    }

    private function getHeaders(string $apiKey)
    {
        return [ "apikey: " . $apiKey, "'Content-Type: application/json'", ];
    }

    public function run(string $apiKey, string $classification) 
    {
        return $this->curl_get(
            $this->getUrl($classification),
            $this->getHeaders($apiKey)
        );
    }

    private function curl_get($url, $headers)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        $reply = curl_exec($ch);
        curl_close($ch);

        return $reply;
    }

}
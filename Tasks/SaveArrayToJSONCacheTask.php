<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use Exception;

class SaveArrayToJSONCacheTask
{
    /** @var RelefoptConfig */
    private $config;
    private const JSON_ENCODE_FLAGS = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;

    public function __construct(RelefoptConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $cache
     * @param string $filename имя файла в который сохранить
     */
    public function run(array $cache, string $cacheFolder, string $filename)
    {
        if (!file_exists($cacheFolder)) {
            $this->createFile($cacheFolder);
        }
        if (!stristr($filename, ".json")) {
            $filename = $filename . ".json";
        }

        $file = fopen($cacheFolder . $filename, 'w');
        fwrite($file, json_encode($cache, self::JSON_ENCODE_FLAGS));
        fclose($file);
    }

    private function createFile(string $directory)
    {
        $permissions = 0777;
        $recursive = true;

        $creatingResult = mkdir($directory, $permissions, $recursive);
        if (!$creatingResult) {
            throw new Exception("Не удалось создать папку для сохранения кэша.");
        }
    }
}
<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Requests\GetProductsListRequest;

class GetProductsListFromApiTask
{
    /** @var GetProductsListRequest */
    private $request;
    /** @var RelefoptConfig */
    private $config;

    public function __construct(RelefoptConfig $config)
    {
        $this->config = $config;
        $this->request = new GetProductsListRequest();
    }

    /**
     * @return array|null
     */
    public function run(int $limit, int $offset)
    {
        $response = $this->request->run(
            $this->config->getApiKey(),
            $limit,
            $offset
        );

        return json_decode($response, 1)['list'] ?? null;
    }

    /**
     * @return int
     */
    public function getCount()
    {
        $response = $this->request->run($this->config->getApiKey(), 1, 0);

        return (int) json_decode($response, 1)['count'];
    }

    public function getLastExceptionDataFromRequest()
    {
        return $this->request->getLastExceptionData();
    }
}
<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use Exception;

class SaveArrayToJSONLogTask
{
    /** @var RelefoptConfig */
    private $config;
    private const JSON_ENCODE_FLAGS = JSON_PRETTY_PRINT | JSON_UNESCAPED_UNICODE | JSON_UNESCAPED_SLASHES;

    public function __construct(RelefoptConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @param array $log
     * @param string $filename имя файла в который сохранить
     */
    public function run(array $log, string $filename)
    {
        $logFolder = $this->config->getLogFolder();
        if (!file_exists($logFolder)) {
            $this->createFile($logFolder);
        }
        if (!stristr($filename, ".json")) {
            $filename = $filename . ".json";
        }

        $file = fopen($logFolder . $filename, 'w');
        fwrite($file, json_encode($log, self::JSON_ENCODE_FLAGS));
        fclose($file);
    }

    private function createFile(string $directory)
    {
        $permissions = 0777;
        $recursive = true;

        $creatingResult = mkdir($directory, $permissions, $recursive);
        if (!$creatingResult) {
            throw new Exception("Не удалось создать папку для сохранения лога.");
        }
    }
}
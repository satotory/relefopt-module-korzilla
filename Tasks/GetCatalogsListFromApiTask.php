<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Requests\GetCatalogsListRequest;

class GetCatalogsListFromApiTask
{
    /** @var GetCatalogsListRequest */
    private $request;
    /** @var RelefoptConfig */
    private $config;

    public function __construct(RelefoptConfig $config)
    {
        $this->config = $config;
        $this->request = new GetCatalogsListRequest();
    }

    /**
     * @return array|null
     */
    public function run()
    {
        $response = $this->request->run(
            $this->config->getApiKey(), $this->config->getClassification()
        );

        return json_decode($response, 1) ?? null;
    }
}
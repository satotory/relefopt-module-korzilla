<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\DTO\RootCatalogDTO;
use App\modules\Korzilla\Relefopt\Data\DTO\SubdivisionDTO;
use App\modules\Korzilla\Relefopt\Data\Models\SubdivisionModel;
use App\modules\Korzilla\Relefopt\Data\Repositories\SubdivisionRepository;

class RemapCatalogsListTask
{
    /** @var SubdivisionRepository */
    private $subRepository;

    /** @var RelefoptConfig */
    private $config;

    public function __construct(
        SubdivisionRepository $subRepository,
        RelefoptConfig $config
    ) {
        $this->subRepository = $subRepository;
        $this->config = $config;
    }

    public function run(array $catalogsList, RootCatalogDTO $rootCatalogDTO) 
    {
        $subs = $this->subRepository->getAllCatalogs();
        
        $remappedList = [
            "relations" => [],
        ];
        foreach ($catalogsList as $catalog) {
            $dto = new SubdivisionDTO;

            /** поля из API */
            $dto->level = (int) $catalog['level'];
            $dto->guid = (string) $catalog['guid'];
            $dto->name = (string) $catalog['name'];
            $dto->parentGuid = (string) $catalog['parentGuid'];
            $dto->description = (string) $catalog['description'];
            $dto->encodedName = $this->encodeString($dto->name);

            /** @var SubdivisionModel|null информация о разделе из базы если она есть */
            $catalogInfo = $subs[$dto->guid] ?? null;
            if (isset($catalogInfo) && !empty($catalogInfo)) {
                $dto->insertOrUpdate = 'update';
                
                $dto->id = $catalogInfo->Subdivision_ID;
                $dto->hiddenUrl = $catalogInfo->Hidden_URL;
                $dto->subClassId = $catalogInfo->getSubClassID();

                $remappedList['relations'][$dto->guid]['Subdivision_ID'] = $dto->id;
                $remappedList['relations'][$dto->guid]['Hidden_URL'] = $dto->hiddenUrl;
                $remappedList['relations'][$dto->guid]['Sub_Class_ID'] = $dto->subClassId;
            } else {
                $dto->insertOrUpdate = 'insert';
                if ($dto->level === 1) {
                    $dto->parentSubId = $rootCatalogDTO->rootSubdivision->Subdivision_ID;
                    $dto->hiddenUrl = $rootCatalogDTO->rootSubdivision->Hidden_URL . $dto->encodedName . "/";
                }
            }
            
            $dto->catalogueId = $this->config->getCatalogue_id();

            $remappedList[$dto->insertOrUpdate][$dto->level][$dto->guid] = $dto;
        }

        return $remappedList;
    }

    private function encodeString($string)
    {
        $table = array(
            'А' => 'a', 'Б' => 'b', 'В' => 'v',
            'Г' => 'g', 'Д' => 'd', 'Е' => 'e',
            'Ё' => 'yo', 'Ж' => 'zh', 'З' => 'z',
            'И' => 'i', 'Й' => 'j', 'К' => 'k',
            'Л' => 'l', 'М' => 'm', 'Н' => 'n',
            'О' => 'o', 'П' => 'p', 'Р' => 'r',
            'С' => 's', 'Т' => 't', 'У' => 'u',
            'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c',
            'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'csh',
            'Ь' => '', 'Ы' => 'y', 'Ъ' => '',
            'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'j', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'csh',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya', '*' => 'x'
        );

        $output = str_replace(array_keys($table), array_values($table), trim($string));
        $output = str_replace("_", "-", $output);
        if (!stristr($output, "http://") && !stristr($output, "https://")) $output = str_replace(" ", "-", trim($output));
        $output = preg_replace("/[^a-zA-Z0-9-]/", "", $output);
        if (is_numeric($output)) $output = "s" . $output;
        
        $output = trim($output, "-");

        return $output;
    }
}
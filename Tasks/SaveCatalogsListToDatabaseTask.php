<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\DTO\SubdivisionDTO;
use App\modules\Korzilla\Relefopt\Data\Models\SubClassModel;
use App\modules\Korzilla\Relefopt\Data\Models\SubdivisionModel;
use App\modules\Korzilla\Relefopt\Data\Repositories\SubClassRepository;
use App\modules\Korzilla\Relefopt\Data\Repositories\SubdivisionRepository;

class SaveCatalogsListToDatabaseTask
{
    /** @var SubdivisionRepository */
    private $subRepository;
    /** @var SubClassRepository */
    private $ccRepository;
    /** @var RelefoptConfig */
    private $config;

    /** @var array массив ключей guid с значением в виде массива параметров id, hiddenUrl */
    private $relations;

    /** @var int возможная глубина разделов */
    private const HIGHEST_LEVEL_TO_HANDLING = 3;

    public function __construct(
        RelefoptConfig $config,
        SubdivisionRepository $subRepository,
        SubClassRepository $ccRepository
    ) {
        $this->config = $config;
        $this->subRepository = $subRepository;
        $this->ccRepository = $ccRepository;
    }

    /**
     * @return array массив ключей guid с значением в виде массива параметров id, hiddenUrl
     */
    public function run(array $catalogsList) 
    {
        $this->relations = &$catalogsList['relations'];

        for ($level = 1; $level <= self::HIGHEST_LEVEL_TO_HANDLING; $level++) {
            if (!isset($catalogsList['insert']) || !$catalogsList['insert'][$level]) continue;

            $this->insertCatalogs($catalogsList['insert'][$level]);
        }

        return $this->relations;
    }

    /**
     * @param SubdivisionDTO[] $catalogsDTOs
     */
    private function insertCatalogs(array &$catalogsDTOs) : void
    {
        foreach ($catalogsDTOs as $catalogDTO) {
            if ($parentSubInfo = $this->relations[$catalogDTO->parentGuid] ?? null) {
                $catalogDTO->parentSubId = $parentSubInfo['Subdivision_ID'];
                $catalogDTO->hiddenUrl = $parentSubInfo['Hidden_URL'] . $catalogDTO->encodedName . "/";
            }

            $subAndCcIds = $this->subRepository->save(SubdivisionModel::modelByDTO($catalogDTO));
            
            $catalogDTO->id = $subAndCcIds['subId'];
            $catalogDTO->subClassId = $subAndCcIds['ccId'];

            $this->relations[$catalogDTO->guid] = [
                'Subdivision_ID' => $catalogDTO->id,
                'Hidden_URL' => $catalogDTO->hiddenUrl,
                'Sub_Class_ID' => $catalogDTO->subClassId,
            ];
        }
    }
}
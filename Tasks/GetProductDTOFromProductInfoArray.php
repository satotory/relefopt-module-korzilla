<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\DTO\ImageDTO;
use App\modules\Korzilla\Relefopt\Data\DTO\PriceDTO;
use App\modules\Korzilla\Relefopt\Data\DTO\ProductDTO;
use App\modules\Korzilla\Relefopt\Data\DTO\StockDTO;

class GetProductDTOFromProductInfoArray
{
    /** @var RelefoptConfig */
    private $config;

    public function __construct(
        RelefoptConfig $config
    ) {
        $this->config = $config;
    }

    public function run(array $info, array $relations) 
    {
        $productDTO = new ProductDTO;
        
        $productDTO->name = $this->cleanString($info['name']);
        $productDTO->description = $this->cleanString($info['description'] ?? "");
        $productDTO->article = (string) $info['code'];
        $productDTO->encodedName = $this->encodeString($productDTO->name . "-" . (string) $productDTO->article);
        $productDTO->vendor = (string) $info['manufacturer']['name'];
        $productDTO->checked = (int) $info['availability'];
        
        $productDTO->guid = (string) $info['guid'];

        $productDTO->imagesArray = $this->getImages($info['images']);
        $productDTO->pricesArray = $this->getPrices($info['prices']);
        $productDTO->stocksArray = $this->getStocks($info['remains']);

        $productDTO->catalogueId = $this->config->getCatalogue_id();
        
        $productDTO->subdivisionId = $relations[$info['section']]['Subdivision_ID'] ?? $this->config->getRootCatalogId();
        
        $productDTO->subClassId = $relations[$info['section']]['Sub_Class_ID'] ?? $this->config->getRootSubClassId();
        
        $subsIds = [];
        foreach ($info['sections'] ?? [] as $section) {
            if ($info['section'] == $section) continue;
            $subsIds[] = $relations[$section]['Subdivision_ID'];
        }

        $productDTO->subdivisionsIds = implode(",", $subsIds) ?? "";
        
        return $productDTO;
    }

    /**
     * @return PriceDTO[]
     */
    private function getPrices($pricesArr)
    {
        $prices = [];

        foreach ($pricesArr as $price) {
            $dto = new PriceDTO;

            $dto->type = (string) $price['type'];
            $dto->value = (float) $price['value'];

            $prices[] = $dto;
        }

        return $prices;
    }

    /**
     * @return StockDTO[]
     */
    private function getStocks($stocksArr)
    {
        $stocks = [];
        
        foreach ($stocksArr as $stock) {
            $dto = new StockDTO;
            
            $dto->store = (string) $stock['store'];
            $dto->main = (bool) $stock['main'];
            $dto->code = (string) $stock['code'];
            $dto->quantity = (int) $stock['quantity'];

            $stocks[] = $dto;
        }
        
        return $stocks;
    }

    /**
     * @return ImageDTO[]
     */
    private function getImages($imagesArr) 
    {
        $images = [];

        foreach ($imagesArr as $image) {
            $dto = new ImageDTO;

            $dto->path = (string) $image['path'];
            $dto->main = $image['name'] == 'Основное';

            $images[] = $dto;
        }

        return $images;
    }

    private function cleanString(string $s) : string
    {
        return (string) addslashes(
            htmlspecialchars_decode(
                str_replace('&amp;', '&', $s)
            )
        );
    }

    private function encodeString($string)
    {
        $table = array(
            'А' => 'a', 'Б' => 'b', 'В' => 'v',
            'Г' => 'g', 'Д' => 'd', 'Е' => 'e',
            'Ё' => 'yo', 'Ж' => 'zh', 'З' => 'z',
            'И' => 'i', 'Й' => 'j', 'К' => 'k',
            'Л' => 'l', 'М' => 'm', 'Н' => 'n',
            'О' => 'o', 'П' => 'p', 'Р' => 'r',
            'С' => 's', 'Т' => 't', 'У' => 'u',
            'Ф' => 'f', 'Х' => 'h', 'Ц' => 'c',
            'Ч' => 'ch', 'Ш' => 'sh', 'Щ' => 'csh',
            'Ь' => '', 'Ы' => 'y', 'Ъ' => '',
            'Э' => 'e', 'Ю' => 'yu', 'Я' => 'ya',
            'а' => 'a', 'б' => 'b', 'в' => 'v',
            'г' => 'g', 'д' => 'd', 'е' => 'e',
            'ё' => 'e', 'ж' => 'zh', 'з' => 'z',
            'и' => 'i', 'й' => 'j', 'к' => 'k',
            'л' => 'l', 'м' => 'm', 'н' => 'n',
            'о' => 'o', 'п' => 'p', 'р' => 'r',
            'с' => 's', 'т' => 't', 'у' => 'u',
            'ф' => 'f', 'х' => 'h', 'ц' => 'c',
            'ч' => 'ch', 'ш' => 'sh', 'щ' => 'csh',
            'ь' => '', 'ы' => 'y', 'ъ' => '',
            'э' => 'e', 'ю' => 'yu', 'я' => 'ya', '*' => 'x'
        );

        $output = str_replace(array_keys($table), array_values($table), trim($string));
        $output = str_replace("_", "-", $output);
        if (!stristr($output, "http://") && !stristr($output, "https://")) $output = str_replace(" ", "-", trim($output));
        $output = preg_replace("/[^a-zA-Z0-9-]/", "", $output);
        if (is_numeric($output)) $output = "s" . $output;
        
        $output = trim($output, "-");

        return $output;
    }
}
<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use App\modules\Korzilla\Relefopt\Data\DTO\ProductDTO;
use App\modules\Korzilla\Relefopt\Data\Models\ProductModel;
use App\modules\Korzilla\Relefopt\Data\Repositories\ProductRepository;

class SaveProductToDatabaseTask
{
    /** @var ProductRepository */
    private $productRepository;

    /** @var RelefoptConfig */
    private $config;

    public function __construct(
        RelefoptConfig $config,
        ProductRepository $productRepository
    ) {
        $this->config = $config;
        $this->productRepository = $productRepository;
    }

    public function run(ProductDTO $productDTO) 
    {
        $product = ProductModel::modelByDTO($productDTO);

        if ($prodInDb = $this->productRepository->getByCode($product->code)) {
            $product->Message_ID = $prodInDb->Message_ID;
        }

        $this->productRepository->save($product);
    }
}
<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Config\RelefoptConfig;
use Exception;

class GetCatalogsListFromCacheTask
{
    /** @var RelefoptConfig */
    private $config;

    public function __construct(RelefoptConfig $config)
    {
        $this->config = $config;
    }

    /**
     * @return array|null
     */
    public function run()
    {
        $cacheFolder = $this->config->getCacheFolder();
        $cacheFilename = $this->config->getCatalogsCacheFileName();

        if (!file_exists($cacheFolder . $cacheFilename)) {
            throw new Exception("Нет кэша каталогов " . $cacheFolder . $cacheFilename);
        }

        $cache = file_get_contents($cacheFolder . $cacheFilename);
        $cacheArr = json_decode($cache, 1);
        if (gettype($cacheArr) != 'array') {
            throw new Exception("В кэше каталогов не хранится массив каталогов");
        }

        return $cacheArr;
    }
}
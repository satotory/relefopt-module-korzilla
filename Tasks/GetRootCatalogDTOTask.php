<?php

namespace App\modules\Korzilla\Relefopt\Tasks;

use App\modules\Korzilla\Relefopt\Data\DTO\RootCatalogDTO;
use App\modules\Korzilla\Relefopt\Data\Repositories\SubClassRepository;
use App\modules\Korzilla\Relefopt\Data\Repositories\SubdivisionRepository;

class GetRootCatalogDTOTask
{
    /** @var SubdivisionRepository */
    private $subRepository;

    /** @var SubClassRepository */
    private $ccRepository;

    public function __construct(
        SubdivisionRepository $subRepository,
        SubClassRepository $ccRepository
    ) {
        $this->subRepository = $subRepository;
        $this->ccRepository = $ccRepository;
    }

    public function run(int $rootSubID) 
    {
        $rootCatalogDTO = new RootCatalogDTO;

        $rootCatalogDTO->rootSubdivision = $this->subRepository->get($rootSubID);
        $rootCatalogDTO->rootSubClass = $this->ccRepository->getBySubdivision_ID($rootSubID);

        return $rootCatalogDTO;
    }
}